const models = require("./models");
const bcrypt = require("bcrypt");

const saveUser = async (req, res) => {
  //create new user details object
  const userDetails = Object.assign({}, req.body);

  //add referred members array
  userDetails.referred = [];
  userDetails.orders = [];
  const { password } = userDetails;
  const hashedPassword = await bcrypt.hash(password, 10);
  userDetails.password = hashedPassword;

  const referral = await models.getReferral(userDetails.referral);

  if (!referral) {
    res
      .status(403)
      .send(
        "We are sorry, no current referral under the email address: " +
          userDetails.referral
      );
  } else {
    const getUser = await models.getUser(userDetails.email);
    if (!getUser) {
      await models.saveUser(userDetails);
      await models.saveReferrer(userDetails.email, userDetails.referral);
      res.status(200).send("User created");
    } else {
      res
        .status(409)
        .send(
          "We already have an application with your email address, we will let you know as soon as possible"
        );
    }
  }
};

const getStock = async (req, res) => {
  const currentStock = await models.getStock();
};

module.exports = { saveUser, getStock };
