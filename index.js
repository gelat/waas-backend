const express = require("express");
const app = express();
var bodyParser = require("body-parser");

var admin = require("firebase-admin");
require("dotenv").config();
require("./models");

const router = require("./routes");
const port = process.env.PORT || 8000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "5mb",
  })
);

app.use((req, res, next) => console.log(req.body));
app.use(router);
app.listen(port, function () {
  console.log("running" + port);
});
