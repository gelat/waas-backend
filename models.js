var admin = require("firebase-admin");

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: process.env.FIREBASE_URL,
  storageBucket: "gs://waas-users",
});

const db = admin.firestore();

//REGISTER
const saveUser = async (user) => {
  return db
    .collection("users")
    .doc(user.email)
    .set(user)
    .then(() => console.log("Success"))
    .catch((error) => console.log(error));
};

const getUser = async (email) => {
  return db
    .collection("users")
    .doc(email)
    .get()
    .then((doc) => {
      return doc.exists;
    })
    .catch((err) => {
      console.log("Error getting document", err);
    });
};

const getReferral = (referral) => {
  return db
    .collection("users")
    .doc(referral)
    .get()
    .then((doc) => {
      return doc.exists;
    })
    .catch((err) => {
      console.log("Error getting document", err);
    });
};

const saveReferrer = async (email, referral) => {
  let getCurrentReferrers = await db
    .collection("users")
    .doc(referral)
    .get()
    .then((doc) => doc.data().referred);
  let user = getCurrentReferrers;
  user.push(email);
  return db
    .collection("users")
    .doc(referral)
    .update({ referred: user })
    .then(() => console.log("Success"))
    .catch((error) => console.log(error));
};

//STOCK INVENTORY

const getStock = async () => {
  const flowers = await db
    .collection("stock")
    .doc("flowers")
    .get()
    .then((doc) => {
      return doc.data();
    });
  const hash = await db
    .collection("stock")
    .doc("hash")
    .get()
    .then((doc) => {
      return doc.data();
    });
  let stock = {};
  stock.flowers = flowers;
  stock.hash = hash;
  return stock;
};

module.exports = { saveUser, getUser, saveReferrer, getReferral, getStock };
